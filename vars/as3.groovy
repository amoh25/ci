def call(dockerRepoName, imageName, portNum){
    pipeline {
        agent any
        stages {
            stage('Build') {
                steps {
                    sh 'pip install -r requirements.txt'
                }
            }
            stage('Lint') {
                steps {
                    sh 'pylint-fail-under --fail_under 5 *.py'
                }
            }
            stage('Security'){
                steps {
                   sh 'pip-audit -r ./requirements.txt'
                }
            }
            
            stage('Package') {
                steps {
                    withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]) {
                        sh "docker login -u 'amoh25' -p '$TOKEN' docker.io"
                        sh "docker build -t ${dockerRepoName}:latest --tag amoh25/${dockerRepoName}:${imageName} ."
                        sh "docker push amoh25/${dockerRepoName}:${imageName}"
                }
            }
}
            stage('Deploy') {
                steps {
                    withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]){
                        sshagent(credentials : ['ssh-creds']) {
                            sh "ssh -o StrictHostKeyChecking=no root@134.122.44.159 'docker login -u amoh25 -p $TOKEN docker.io'"
                            //sh "ssh -o StrictHostKeyChecking=no root@134.122.44.159 'cd /root/lab6/ && docker compose down'"
                            sh "ssh -o StrictHostKeyChecking=no root@134.122.44.159 'docker pull amoh25/${dockerRepoName}:${imageName}'"
                            sh "ssh -o StrictHostKeyChecking=no root@134.122.44.159 'cd /root/lab6/ && docker compose up -d'"
                        }
                    }

                }
            }
        }
    }

}
